package de.robin;
import java.util.Properties;

import kafka.consumer.ConsumerConfig;
import kafka.consumer.ConsumerIterator;
import kafka.consumer.KafkaStream;
import kafka.javaapi.consumer.ConsumerConnector;
import kafka.producer.KafkaLog4jAppender;
import kafka.utils.ZkUtils;
 
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;
 
/**
 * Star a consumer that reads with a certain number of threads from a kafka server.
 * Parameters:
 * - Zookeeper address
 * - consumer_group
 * - topic
 * - treads
 * 
 * 
 * Example:
 * java -cp /tmp/uber-robin.Kafka-0.0.1-SNAPSHOT.jar de.robin.KafkaConsumer localhost my_group my_topic 5
 * @author alyr
 *
 */
public class KafkaConsumer {
	public static final Logger logger = Logger.getLogger(KafkaConsumer.class);    
			
    private final ConsumerConnector consumer;
    private final String topic;
    private  ExecutorService executor;
	private String group;
 
	/*
	 * Worker
	 */
    private static class ConsumerTest implements Runnable {
        private KafkaStream m_stream;
        private int m_threadNumber;
     
        public ConsumerTest(KafkaStream a_stream, int a_threadNumber) {
            m_threadNumber = a_threadNumber;
            m_stream = a_stream;
        }
     
        public void run() {
            ConsumerIterator<byte[], byte[]> it = m_stream.iterator();
            System.out.println("Starting thread " + m_threadNumber); 
            while (it.hasNext()) {
            	logger.info("Consumer Thread " + m_threadNumber + ": " + new Integer(it.next().message()));
            }
            System.out.println("Shutting down Thread: " + m_threadNumber);
        }
    }
    
    /* 
     * Controler Class
     * 
     */
    public KafkaConsumer(String a_zookeeper, String a_groupId, String a_topic) {    	
        consumer = kafka.consumer.Consumer.createJavaConsumerConnector(
                createConsumerConfig(a_zookeeper, a_groupId));
        this.topic = a_topic;
        this.group = a_groupId;
    }
    
    private static ConsumerConfig createConsumerConfig(String a_zookeeper, String a_groupId) {
        Properties props = new Properties();
        props.put("zookeeper.connect", a_zookeeper);
        props.put("group.id", a_groupId);
        props.put("zookeeper.session.timeout.ms", "400");
        props.put("zookeeper.sync.time.ms", "200");
        props.put("auto.commit.interval.ms", "1000");
        return new ConsumerConfig(props);
    }
 
    public void shutdown() {
        if (consumer != null) consumer.shutdown();
        if (executor != null) executor.shutdown();
    }
 
    public void run(int a_numThreads) {
        Map<String, Integer> topicCountMap = new HashMap<String, Integer>();
        topicCountMap.put(topic, new Integer(a_numThreads));
        System.out.println(topic + ": " + a_numThreads);
        
        //long readOffset = getLastOffset(consumer, topic, 0, kafka.api.OffsetRequest.EarliestTime(), group);
        
        Map<String, List<KafkaStream<byte[], byte[]>>> consumerMap = consumer.createMessageStreams(topicCountMap);
        List<KafkaStream<byte[], byte[]>> streams = consumerMap.get(topic);
 
        // now launch all the threads
        //
        executor = Executors.newFixedThreadPool(a_numThreads);
 
        // now create an object to consume the messages
        //
        int threadNumber = 0;
        for (final KafkaStream stream : streams) {
            executor.submit(new ConsumerTest(stream, threadNumber));
            threadNumber++;
        }
    }
 
    public static void main(String[] args) {
        String zooKeeper = args[0];
        String groupId = args[1];
        String topic = args[2];
        int threads = Integer.parseInt(args[3]);
 
        KafkaConsumer example = new KafkaConsumer(zooKeeper, groupId, topic);
        example.run(threads);
 
        try {
            Thread.sleep(10000);
        } catch (InterruptedException ie) {
 
        }
        example.shutdown();
    }
}
