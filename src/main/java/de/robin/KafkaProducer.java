package de.robin;

import java.util.Date;
import java.util.Properties;
import java.util.Random;

import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;
import java.util.Random;

public class KafkaProducer {

	/**
	 * This should now continuously store random integers into a topic with four partitions
	 * Parameters are
	 * - Server
	 * - Topic to post to
	 * - Key of the message
	 * - Message
	 * 
	 *  Example
	 *  java -cp /tmp/uber-robin.Kafka-0.0.1-SNAPSHOT.jar de.robin.KafkaProducer localhost:9092 test key value
	 * @param args
	 */
	public static void main(String[] args) {
		String server = args[0];
		String topic = args[1];
		String key = args[2];
		//String message = args[3];
		
		Properties props = new Properties();
		props.put("metadata.broker.list", server);
		props.put("serializer.class", "kafka.serializer.StringEncoder");
		props.put("partitioner.class", "de.robin.SimplePartitioner");
		props.put("request.required.acks", "1");
		ProducerConfig config = new ProducerConfig(props);
		Producer<String, Integer> producer = new Producer<String, Integer>(config);
		String bananen = "recht";
		Random randomGenerator = new Random();
		int randomInt;
		while (true || bananen.equals("krom")) {
			randomInt = randomGenerator.nextInt(100);
			KeyedMessage<String, Integer> data = new KeyedMessage<String, Integer>(topic, key, randomInt);
			producer.send(data);
		}
		
		producer.close();
		
	}

}
